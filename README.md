# SyncEvent: Linux Kernel Synchronization Primitive

I worked with a partner in this project and we engaged in pair programming. 

### High Level Description
This project involved adding a synchronization primitive called a SyncEvent to the linux kernel and writing user-level code to test the SyncEvents. 

Processes share SyncEvents to synchronize their actions. A process may open a new or existing sync event using se_open, which adds it to a linked list of processes synchronizing on that SyncEvent. Multiple processes synchronize their actions by opening the same SyncEvent and calling se_wait and se_signal. 

### Implementation Details
The SyncEvents are stored in a fixed size array of 5 events.
Each SyncEvent stores:

* int id identifier tag
* a valid flag to track whether the SyncEvent is open or closed
* a custom ProcessNodeList struct to store the process-specific data held in each node of the linked list.
    *  The ProcessNodeList struct contains:
        * list_head referring to nodes of the linked list
        * pid_t to track the pid of the process holding the SyncEvent.
        * int flag indicating if the process is blocked
* a wait queue head to track the wait queue (linux kernel data structure) of processes waiting on a sync event
* a list_head struct to store a linked list of pids of the processes that have SyncEvents open


The following functions define possible operations on a Sync Event:

* long se_open(int *id)
    * if id == 0, create a new SyncEvent. 
    * if 0 < id < 5, open an existing SyncEvent with the matching id value
* long se_wait(int id)
    * blocks the calling process on the SyncEvent with a matching id 
* long se_signal
    * wakes up all processes blocked on the event with matching id
* long se_close(int id)
    * Close SyncEvent with the corresponding id. The last close to a SyncEvent destroys the SyncEvent 
* long print_se_table()
    * prints out contents of SyncEvent table. 
    * for open events, prints process ids of every process that has the event open and the pid of all processes waiting on each event