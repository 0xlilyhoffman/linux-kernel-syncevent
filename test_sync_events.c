/*
 * user-level program for testing new SyncEvent synchronization
 * primative added to linux kernel.
 */

#include <stdlib.h>
#include <sys/syscall.h>
#include <asm/unistd.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>


/*
 * if you want to use the __NR_se_open definitions instead of the exact
 * system call numbers (like 312) in your calls to syscall from your 
 * test program, change the INCLUDES definition in the Makefile to include
 * the arch/x86/include path too
 */
#define se_open(arg1) syscall(312, arg1)
#define se_close(arg1) syscall(313,arg1)
#define print_se_table() syscall(314)
#define se_wait(arg1) syscall(315, arg1)
#define se_signal(arg1) syscall (316, arg1)

int call_signal(int id);
int call_wait(int id);
int call_close(int id);
int call_open(int id);
int print_table();
void run_all();
void menu(int *option, int *id);

int main() {
	int quit = 1;
	while(quit == 1) {
		int option = 0;
		int id = 0;
		int ret = 0;
		menu(&option, &id);
		switch(option) {
			case(1):
				ret = call_open(id);
				break;
			case(2):
				ret = call_close(id);
				break;
			case(3):
				ret = call_wait(id);
				break;
			case(4):
				ret = call_signal(id);
				break;
			case(5):
				ret = print_table();
				break;
			case(6):
				run_all();
				break;
			case(7):
				quit = 0;
				break;
		}
		if(ret != 0) {
			printf("AN ERROR OCCURED\n");
		}
	}
	return 0;
}

void menu(int *option, int *id) {
	*option = 0;
	do {
		printf("=================\n");
		printf(" 1) open\n");
		printf(" 2) close\n");
		printf(" 3) wait\n");
		printf(" 4) signal\n");
		printf(" 5) print\n");
		printf(" 6) run all\n");
		printf(" 7) quit\n");
		printf("=================\n");
		scanf("%d", option);
		if(*option < 5) {
			printf("Sync_event #?\n");
			scanf("%d", id);
		} else {
			*id = 0;
		}
	}while(*option <= 0 && *option > 7);
}

int call_open(int id) {
	printf("Open\n");
	int ret = se_open(&id);
	return ret;
}

int call_close(int id) {
	printf("Close\n");
	int ret = se_close(id);
	return ret;
}

int call_wait(int id) {
	printf("Wait\n");
	int p = fork();
	if(p == 0) {
		se_wait(id);
		printf("I WAITED\n");
		exit(0);
	} else {
		sleep(2);
	}
	return 0;
}

int print_table() {
	printf("Print Table\n");
	int ret = print_se_table();
	return ret;
}

int call_signal(int id) {
	printf("Signal\n");
	int ret = se_signal(id);
	return ret;
}

void run_all() {
	/*
	TEST CASES:
	* Multiple processes open an event.									✓
	* One process waiting when se_signal is called.						✓
	* No processes waiting when se_signal is called.					✓
	* Multiple processes waiting when se_signal is called.				✓
	* More than max number of new SyncEvent opens are attempted.		✓
	* One process closes a SyncEvent which has multiple opens.			✓
	* The last process closes a SyncEvent.								✓
	* Trying to open/close/signal/wait on an invalid SyncEvent number.	✓
	*/
	int i = 0;
	int k = 1;
	int z = 4;
	// TEST CASE 1
	// TEST CASE 3
	printf("Running test case 1 and 3\n");
 	se_open(&i);
	se_open(&i);
	se_signal(i);
	se_close(i);
	print_se_table();

	//Append process to first sync event
	//Table[0] open with 2 processes
	se_open(&k);
	print_se_table();

	//Delete a process from a sync event in slot 1
	//Table[0] open with 1 process
	print_se_table();		
	
	//Process in table[0] waits
	printf("About to wait\n");
	
	int p = fork();
	if(p == 0) {
		se_open(&k);
		se_wait(k);
		printf("I WAITED\n");
		exit(0);
	} else {
		sleep(5);
	}
	// TEST CASE 2
	printf("Running test case 2\n");
	se_signal(k);

	// TEST CASE 4
	printf("Running test case 4 & 6\n");

	p = fork();
	if(p == 0) {
		se_open(&z);
		se_wait(z);
		se_close(z);
		exit(0);
	} 	
	p = fork();
	if(p == 0) {
		se_open(&z);
		se_wait(z);
		se_close(z);
		exit(0);
	}	
	sleep(1);
	se_signal(z);
	se_close(z);
	print_se_table();

	int a = 0;
	int b = 0;
	int c = 0;
	int d = 0;


	//Open 4 more Sync Events
	se_open(&a);
	se_open(&b);
	se_open(&c);
	se_open(&d);
	printf("Running test case 5\n");
	print_se_table();

	
	//Open 6th SyncEvent
	int f = 0;
	se_open(&f);
	print_se_table();

	// INVALID TESTS
	printf("Running test case 7\n");
	int y = 7;
	se_open(&y);
	se_close(y);
	se_signal(y);
	se_wait(y);
	printf("DIDN'T PANIC KERNEL!\n");
}
