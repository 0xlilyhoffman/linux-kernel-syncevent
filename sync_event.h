#ifndef _SYNC_EVENT_H
#define _SYNC_EVENT_H

/* kernel + user space stuff */

/*
 * @param list_head  list - refererence to nodes of the linked list
 * @param pid_t noepid  - to track the pid of the process holding the SyncEvent.
 * @param int sleeping - flag indicating if the process is blocked
 */
struct ProcessNodeList {
	int sleeping;
	struct list_head list;
	pid_t nodepid;
};

/*
 *The SyncEvents are stored in a fixed size array of 5 events.
 * @param: int id - identifier tag
 * @param ProcessNodeList pid_list - ProcessNodeList struct to store the process-specific data held in each node of the linked list
 * @param wait_queue_head_t *waitqueue_head: a wait queue head to track the wait queue of processes waiting on a sync event
 * @param list_head pid_list_head - a list_head struct to store a linked list of pids of the processes that have SyncEvents open
 * @param int valid - a valid flag to track whether SyncEvents are open or closed
 */
struct SyncEvent {
	int id;
	wait_queue_head_t *waitqueue_head;
	struct ProcessNodeList pid_list;
	struct list_head pid_list_head;
	int valid;
};

#ifdef __KERNEL__
/*
 * everthing inside here is only visable (available) at kernel level
 * if you want definitions that are shared between kernel and user
 * level programs, those need to be outside the #ifdef __KERNEL__ #endif
 */

#include <linux/init.h>

extern void __init se_init(void);
// se_open, se_close...



#endif  /* __KERNEL__ */
#endif
