/*
 * SyncEvent synchronization primitive in Linux Kernel
 *
 * Processes share SyncEvents to synchronize their actions
 * A process may open a new or existing sync event using se_open, which adds it to a linked list of processes synchronizing on that SyncEvent.
 * Multiple processes synchronize their actions by opening the same SyncEvent and calling se_wait and se_signal.
 */

#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/semaphore.h>
#include <linux/wait.h>
#include <linux/errno.h>
#include <asm/current.h>
#include <linux/syscalls.h>


#include <linux/sync_event.h>

struct SyncEvent eventTable[5];

/*
 * Initialize a table of 5 SyncEvents
 */
void __init se_init(void) {
	int i;
	for(i = 0; i < 5; i++) {
		//set id, valid bit, & state
		eventTable[i].valid = 0;
		eventTable[i].id = i+1;

		// Declare/init a new waitqueue head 	
		eventTable[i].waitqueue_head = kmalloc(sizeof(wait_queue_head_t), GFP_KERNEL);
		init_waitqueue_head(eventTable[i].waitqueue_head);

		//Declare/init a new list head
		INIT_LIST_HEAD(&eventTable[i].pid_list_head);
	}
}

/*
 * Open a new/exiting Sync event - a process opening a SyncEvent adds itself to the list of processes using that SyncEvent
 *
 * If id==0, create a new SyncEvent, and set the id to the new SyncEvent’s ID.
 * If id is positive, open an existing SyncEvent with a matching id value.
 */
SYSCALL_DEFINE1(se_open, int __user *, id){
	//Check id okay to read from and write to
	if(!access_ok(VERIFY_WRITE, id, sizeof(int)))
		return EROFS;
    
	//valid table_index 01234
	int table_index = (*id)-1;

	//if id in table, create node and append to list
	if(table_index >= 0 && table_index < 5){
		// Checking if this process already opened this SyncEvent
		struct ProcessNodeList *nodeptr;
		int found = 0;
		list_for_each_entry(nodeptr, &eventTable[table_index].pid_list_head, list){
			if(nodeptr->nodepid == (get_current())->pid){
				found += 1;
			}
		}
		if(found > 0) {
			return EPERM;
		}
		printk("id in table, creating node to prepend to list\n");
        
		//Open SyncEvent by setting valid = 1
		eventTable[table_index].valid = 1;

		//Create ProcessNode to add to linked list of processes in SyncEvent
		struct ProcessNodeList* processNodeListJ = kmalloc(sizeof(struct ProcessNodeList), GFP_KERNEL);
		processNodeListJ->nodepid = (get_current())->pid;
		processNodeListJ->sleeping = 0;
		INIT_LIST_HEAD(&processNodeListJ->list);

		//Append ProcessNode to linked list of processes in SyncEvent
		list_add(&processNodeListJ->list, &eventTable[table_index].pid_list_head);
	}
    //Open new SyncEvent
	else if(table_index == -1){
		printk("id == 0, so creating new sync event...\n");
		int i;
		int new_empty_slot_found = 0;
		for(i = 0; i < 5; i++){
			//if open slot
			if(eventTable[i].valid == 0){

                // Checking if this process already opened this SyncEvent
				struct ProcessNodeList *nodeptr;
				int found = 0;
				list_for_each_entry(nodeptr, &eventTable[i].pid_list_head, list){
					if(nodeptr->nodepid == (get_current())->pid){
						found += 1;
					}
				}
				if(found > 0) {
					return 0;
				}
				
				printk("Creating new sync event with id %d\n", i+1);
				new_empty_slot_found = 1;
				
				//Set id to new SyncEvent ID
				*id = eventTable[i].id;
				copy_to_user(&id, &eventTable[i].id, sizeof(int*));

				//Create new SyncEvent
				eventTable[i].valid = 1;
				printk("Set valid bit of %d to %d\n", eventTable[i].id, eventTable[i].valid);
				
				//Create ProcessNode to add to linked list of processes in SyncEvent[i]
				struct ProcessNodeList* processNodeListJ = kmalloc(sizeof(struct ProcessNodeList), GFP_KERNEL);
				processNodeListJ->nodepid = (get_current())->pid;
				processNodeListJ->sleeping = 0;
				INIT_LIST_HEAD(&processNodeListJ->list);
				
				printk("Adding node to list \n");
                
				//Create node and append
				list_add(&processNodeListJ->list, &eventTable[i].pid_list_head);
				
				//Break after first empty slot found
				break;
			}
		}
		//If no empty SyncEvents available, 
		//return "Operation not permitted error"
		if(new_empty_slot_found == 0){
			printk("ERROR: No room for new SyncEvent\n");	 
			return EPERM; 
		}
	}
	else {
		printk("Index %d not found\n", *id);
		return EPERM;
	}
	return 0;
}

/*
 * Prints out the contents of the event table.
 * For open events, includes the process ids of every process that has the event open,
 * and prints out the pid of all processes waiting on each event.
 */
SYSCALL_DEFINE0(print_se_table) {
	//Print open process pids
	int i;
    printk("SyncEvent table:\n");
	printk("========================================================\n");

	for(i = 0; i < 5; i++){
		//If open event
		if(eventTable[i].valid == 1){
			printk("Open Sync Event: #%d valid : %d \n", eventTable[i].id, eventTable[i].valid);
			//Iterate through pid and print pids
			struct ProcessNodeList *nodeptr;
			list_for_each_entry(nodeptr, &eventTable[i].pid_list_head, list){
				if(nodeptr == NULL) break;
				printk("Process id: %d open\n", nodeptr->nodepid);
				if(nodeptr->sleeping == 1){
					printk("Process id: %d waiting \n", nodeptr->nodepid);
				}
			}
		}
		else {
			printk("Closed Sync Event #%d valid: %d\n", eventTable[i].id, eventTable[i].valid);
		}
		printk("========================================================\n");
	}
	return 0;
}


/*
 * Blocks the calling process on the SyncEvent with a matching id
 * NOTE: caller must have opened the SyncEvent before it can wait or signal or close it
 */
SYSCALL_DEFINE1(se_wait, int __user, id){
	int table_index = id - 1;

	printk("Process #%d waiting... \n", eventTable[table_index].id);
	//If invalid id return error
	if(id < 1 || id > 5)
		return EPERM;
	
	//If SyncEvent id not open, error
	if(eventTable[table_index].valid == 0)
		return EPERM;

	//Check to see if calling process HAS a SyncEvent
	int found = 0;
	if(eventTable[table_index].valid == 1){
		struct ProcessNodeList *nodeptr;
		list_for_each_entry(nodeptr, &eventTable[table_index].pid_list_head, list){
			if(nodeptr->nodepid == (get_current())-> pid){
				found = 1;
				nodeptr->sleeping = 1;
				printk("Process DOES have a SyncEvent");
			}
		}
	}
	
	//If calling process does not have SyncEvent open, error
	if(found == 0) {
		printk("Waiting process does not have SyncEvent");
		return EPERM;
	}
	
	//HERE implies SyncEvent id is open and calling process has it
	//Block calling process by adding it to wait queue
	//(1) Set process state in task_struct to TASK_INTERRUPTIBLE
	set_current_state(TASK_INTERRUPTIBLE);

	//(2) Add process to wait queue associated with SyncEvent id
	//(2a) delcare and initilaize a wait_queue_t element
	struct task_struct *curtask = get_current();
	DECLARE_WAITQUEUE(wait_entry, curtask);

	//(2b)Add process to wait queue associated with that SyncEvent
	add_wait_queue(eventTable[table_index].waitqueue_head, &wait_entry);

	//(3) Call kernel schedule function to preempt the process and schedule
	//another process form the READY Q to run
	schedule();
	return 0;
}

/*
 * Wakes up all processes blocked on the event with the matching id.
 * Only processes that have opened the SyncEvent can signal on it.
 */
SYSCALL_DEFINE1(se_signal, int __user, id){
	printk("ENTERING SIGNAL\n");
	int table_index = id -1;
	//If invalid id, error
	if(id < 1 || id > 5){
		printk("Error, invalid id \n");
		return EPERM;
	}
	
	///If sync event id not open, error
	if(eventTable[table_index].valid = 1){
		printk("Sync event id not open \n");
		return EPERM;
	}

	//Check to see if calling process HAS a sync event
	int found = 0;
	struct ProcessNodeList *nodeptr;
	if(eventTable[table_index].valid == 1){
		printk("Calling process DOES have a sync event \n");
		list_for_each_entry(nodeptr, &eventTable[table_index].pid_list_head, list){
			nodeptr->sleeping = 0;
			printk("All woken up \n");
			if(nodeptr->nodepid == (get_current())->pid){
				found = 1;
			}
		}
	}

	//If calling process doesn't have SyncEvent open, error
	if(found == 0){
		printk("Calling process doesn't have a SyncEvent open \n");
		return EPERM;
	}

	wake_up(eventTable[table_index].waitqueue_head);
	return 0;
}

/*
 * Closes the corresponding SyncEvent.
 * The last close to a SyncEvent, destroys the SyncEvent.
 * Only processes that have the SyncEvent open can close the SyncEvent.
 * The last close fails if there are waiters on the SyncEvent
 */
SYSCALL_DEFINE1(se_close, int __user, id){
	int table_index = id - 1;
	printk("Entering close...\n");
	
	//If id value not fitting in table return error
	if(id < 1 || id > 5)
		return EPERM;

	//If corresponding SyncEvent does not exist, return error
	if(eventTable[table_index].valid == 0){
		return EPERM;
	}

	//If sync event exists to be closed:
	//Delete event form linked list and free its entry
	if(eventTable[table_index].valid == 1){
		printk("Closing process from event %d \n", id);
		//Find entry to delete from linked list
		struct ProcessNodeList *nodeptr;
		struct ProcessNodeList *tmp;
		list_for_each_entry_safe(nodeptr, tmp, &eventTable[table_index].pid_list_head, list){
			if(nodeptr->nodepid == (get_current())->pid){
				list_del(&nodeptr->list);
				kfree(&nodeptr);
				break;
			}
		}
	}

	if(list_empty(&eventTable[table_index].pid_list_head)){
		printk("List empty, closing entire SyncEvent %d \n", id);
		eventTable[table_index].valid = 0;
	}
	return 0;

}
